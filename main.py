from selenium import webdriver
import time
 
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/local/bin/chromedriver"
driver = webdriver.Chrome()
driver.get('https://python.org')
